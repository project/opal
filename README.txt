Installation/config guide:
---------------------------
1. File list:
- README - this file
- opal.install - installation instructions for Drupal
- css/opal.css - formatting for settings panel
- opal.module - opal module php code
- opal.info - plugin info for Drupal
- images/
  + bar.png
  + opal_bar.png
  + neg_bar.png
  + pos_bar.png
  + positive.png
  + negative.png
  + neutral.png
  
2. Installation
- copy the entire "opal" directory into your drupal modules directory (e.g. <your drupal direcotry>/sites/all/modules/)
- download the SentiWordNet database "opal.sentiwordnet" from http://www.fernandotapiarico.com/apps/docs/opal/opal.sentiwordnet
- copy "opal.sentiwordnet" in your "opal" directory (e.g. <your drupal direcotry>/sites/all/modules/opal/opal.sentiwordnet)
- go to the Drupal administration panel (modules panel), find OPAL and active it
- the OPAL settings panel allows to configure the plugin. It is available under Site Configuration -> Opinion Analizer 


Credits
-------------------
The application has been developed by Fernando Tapia Rico (http://www.fernandotapiarico.com/) for the Gi2MO Project (http://www.gi2mo.org/). 


Version notes:
-------------------

* OPAL 1.4
- Added Microsoft translation service instead of Google Translation service

